import CreateUser from "../features/user/CreateUser";

function Home() {
  return (
    <div className="my-10 sm:my-16 text-center">
      <h1 className="mb-8 text-3xl tracking-widest">
        The best pizza.
        <br />
        <span className="text-pizza">
          Straight out of the oven, straight to you.
        </span>
      </h1>
      <CreateUser />
    </div>
  );
}

export default Home;
