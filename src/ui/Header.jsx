import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import SearchOrder from "../features/order/SearchOrder";

const Header = () => {
  const userName = useSelector((state => state.user.userName));
  return (
    <header className="bg-yellow-400 border-b border-stone-400 font-pizza flex justify-around items-center py-3">
      <Link to="/" className="text-xl bg-stone">
        Fast Pizza Go
      </Link>
      <SearchOrder/>
      <h1 className="hidden sm:block">{userName}</h1>
    </header>
  );
};

export default Header;
