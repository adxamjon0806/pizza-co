import React from "react";
import Button from "../../ui/Button";
import { useDispatch } from "react-redux";
import { decreaseCart, increaseCart } from "./cartSlice";

const UpdateCartQty = ({id, itemQuantity}) => {
  const dispatch = useDispatch();

  return (
    <div className="flex gap-3 items-center">
      <Button type="small" onClick={() => dispatch(increaseCart(id))} >+</Button>
      <span>{itemQuantity?.quantity}</span>
      <Button type="small" onClick={() => dispatch(decreaseCart(id))} >-</Button>
    </div>
  );
};

export default UpdateCartQty;
