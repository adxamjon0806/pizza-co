import React from 'react'

const Loader = () => {
  return (
    <div className='absolute bg-slate-200/30 backdrop-blue-sm flex items-center justify-center '>
      <div className='loader'></div>
    </div>
  )
}

export default Loader