/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx,}"],
  theme: {
    extend: {
      fontFamily: {
        pizza: "Roboto mono, monospace",
      },
    },
    screens: {
      'sm': '640px',
      'md': '768px',
    },
  },
  plugins: [],
};
