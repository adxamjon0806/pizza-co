import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

const SearchOrder = () => {
    const [query, setQuery] = useState("")
    const navigate = useNavigate();

    function handleSubmit(e) {
        e.preventDefault();
        if (!query) return;
        navigate(`/order/${query}`)
        setQuery("");
    }
  return (
    <form onSubmit={handleSubmit}>
        <input 
         className='rounded-full focus:outline-none w-40 bg-yellow-50 py-1 px-2 sm:py-2 sm:px-4 sm:w-52'
         placeholder='Search Order #' 
         value={query} 
         onChange={(e) => setQuery(e.target.value)}></input>
    </form>
  )
}

export default SearchOrder
