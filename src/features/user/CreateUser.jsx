import { useState } from "react";
import Button from "../../ui/Button";
import { useDispatch, useSelector } from "react-redux";
import { updateUserName } from "./userSlice";
import { useNavigate } from "react-router-dom";

function CreateUser() {
  const navigate = useNavigate()
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const name = useSelector((state) => state.user.userName)

  function handleSubmit(e) {
    e.preventDefault();
    if (username) {
      dispatch(updateUserName(username))
      navigate("/menu")
      setUsername("")
    }
  }
  if(name){
    return <Button type="primary" to="/menu">Continue ordering, {name}</Button>
  }

  return (
    <form onSubmit={handleSubmit}>
      <p className=" mb-4 text-sm text-stone-600  md:text-base">
        👋 Welcome! Please start by telling us your name:
      </p>

      <input
        className="mb-4 p-3 bg-stone-200 rounded-full"
        type="text"
        placeholder="Your full name"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />

      {username !== "" && (
        <div>
          <Button type='primary' >
            Start ordering
          </Button>
        </div>
      )}
    </form>
  );
}

export default CreateUser;
