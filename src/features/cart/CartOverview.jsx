import { useSelector } from "react-redux";
import { getTotalCartQuantity, getTotalPriceQuantity } from "./cartSlice";
import { formatCurrency } from "../../utils/helpers";
import { Link } from "react-router-dom";
function CartOverview() {
  const totalCartQuantity = useSelector(getTotalCartQuantity);
  const totalPrice = useSelector(getTotalPriceQuantity);
  return (
    <div className="bg-stone-900 text-white flex items-center justify-between px-3 py-4 uppercase font-Robo text-sm sm:px-10 sm:text-lg">
      <p className="flex space-x-4">
        <span>pizzas : {totalCartQuantity} </span>
        <span className="flex"> <p className="hidden sm:block">Total Price : </p>  {formatCurrency(totalPrice)}</span>
      </p>
      <Link to="/cart">Open cart &rarr;</Link>
    </div>
  );
}

export default CartOverview;
